// to plot rayleigh jeans law, weins law and cmapre them with planck raditin law
clc
clear
clf
h=6.6e-36
c=3e8
k=1.38e-23
T=[3000,5000,70000]
v=(0.1:0.1:15)*10^14
s=['--k','-or','+b']
// rayleigh jeans law as a functin of frequency
function y =r(x,T)
    y=8*%pi*k*T*x^2/c^3
endfunction
//weiens law as function of frequency
function y=W(x,T)
    y=8*%pi*h*x^3./(c^3.*exp(h*x/(k*T)))
endfunction
//planck radition law as a function of frequency
function y=P(x,T)
    y=8*%pi*h*x^3./(c^3.*(exp(h*x/(k*T)-1)))
endfunction
 //for rayleigh jeans law
scf(0)
for i= 1:3
    y1=r(v,T(i))
    plot(v,y1,s(i))
end
xtitle ('rayleigh jeans law','frequency (hz)','energy density (J/m^3)')
legend('t=3000k','T=5000k','T=7000K')
 //for weins distributin law
scf(1)
for i = 1:3
    y2=W(v,T(i))
    plot(v,y2,s(i))
    
end
xtitle('weins distribution  law','frequency(hz)','energy density')
scf(2)
for i= 1:3
    y3=P(v,T(i))
    plot(v,y3,s(i))
end
// comparision
scf(3)
g1=P(v,5000)
g2=r(v,5000)
g3=W(v,5000)
plot(v,g1,s(1))
plot(v,g2,s(2))
plot(v,g3,s(3))
a=gca()
a.data_bounds=[0,0;15D14,1D-15]
