clc
clear
clf
k=1.38e-23
n=6e-23
M=[1 4 6]
m=M.*1.667e-27
T=[100 200 300]
v=[0:5000]
for i =1:3
    for j=1:length(v)
        f(j)=4*%pi*(m(i)/(2*%pi*k*T(1)))^(3/2)*(v(j)^2)*exp(-m(i)*(v(j)^2)/(2*k*T(1)))
end
scf(2)
plot(v',f)
end
// finding rms, average, mp speed
O=32*1.67e-27
rms=sqrt(2*k*T(2)/(O))
disp("rms speed is "+string(rms)+"rms")
