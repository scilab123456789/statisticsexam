clc;clear;clf
h=6.6e-34
c=3e8
k=1.38e-23
b=2.89e-3
x=[600 1200 1700 2300 2800 3400]*1e-9
u= [1.67 4.37 3.04 1.77 1.04 0.63]*1d11
plot(x,u,'or')
function y = f(x,t)
    y=2*h*c^2./(x^5.*(exp((h*c)./(x.*k*t))-1))
endfunction
n1=find(u==max(u))
xmax=x(n1)
// using weins law
tmax=b/xmax
t=tmax-1000:1:tmax+1000
for i=1:length(t)
    for j=1:length(x)
        er(i,j)=u(j)-f(x(j),t(i))
end
// sum of square of error
s(i)=sum(er(i,:).*er(i,:))
end
n=find(s==min(s))
T=t(n)
disp("for T"+string(T)+"K, plancks radiation law least square fits the given data")
disp("hence temp of body is" +string(T)+"k")
v=(1:10:4000)*1e-9
plot(v,f(v,t(n)),'-k')

