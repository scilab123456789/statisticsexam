clc
clear
clf
ev=1.69e-19
u=0 //Chemical Potential
k=1.38e-23 //Boltzmann Constant
T=100:200:500 //Temperature
kt=k.*T
E=-0.1:0.001:1 //Energy Range
a=-1
dist=["Bose-Einstein Distribution","Maxwell Boltzman distribution","Fermi Dirac distribution"]
for i=1:3
    for j=1:length(T)
        for r = 1:length(E)
            f(j,r)=1/(exp(((E(r)-u)*ev)/kt(j))+a)
            //f(j,i)=1/(exp(((E(i)-u)*ev)/kt(j))+a)
        end
    end
    scf(0)
    subplot(2,2,i)
    
    plot(E',f')
    xtitle(dist(i),"energy(ev)","f(v)")
    legend(["T = 100k","T=200K","T=300k"])
    
    a=a+1       
    c(i,:)=f(2,:)          
end
subplot(2,2,4)
plot(E',c')

