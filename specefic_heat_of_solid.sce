clc;clear;clf
R=8.314 // gas constant 
t=[1 :5:400]
// dulong petit law
cv1=3*R
plot(t,cv1,'-*k') 
te=100
// einstein law
function y= f(t)
    x=te./t
    y=3*R*x^2.*exp(x)./((exp(x)-1)^2)
endfunction
cv2=f(t)
plot(t,cv2,'-.r')
// debye law
td= 100
function y=g(t)
    d=td./t
    y=9*R.*d^-3.*(integrate('y^4.*exp(y)./(exp(y)-1)^2','y',0,d))
endfunction
cv3=g(t)
plot(t,cv3,'-<bl')



