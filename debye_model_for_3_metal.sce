clc
clear
clf()
k=1.38e-23
N=6.02e23
Td=[645;343;105]
T=1:5:700
R=N*k
clr=['-*r','-og','-dc']
for j=1:length(Td)
 for i=1:length(T)
 I=integrate('((y^4)*exp(y))/((exp(y)-1)^2)','y',0,(Td(j)/T(i)))
 Cvd(i,j)=3*((T(i)/Td(j))^3)*I // Debye Model
 end
 plot(T',Cvd(:,j),clr(j))
end
replot([0,0,700,1.2])
legend(['For Si','For Cu','For Pb'],[4])
xtitle('Debye Specific Heat Model','Temperature(K)','Cv/3R')
