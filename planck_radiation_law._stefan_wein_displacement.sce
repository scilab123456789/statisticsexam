clc
clear
clf()
h=6.6e-34
c=3e8
k=1.38e-23
T=[400 500 600]
s=['-or','-<bl','+k']
l=(0.1:0.01:20)*1e-6
v=(0.01:0.001:1)*1e14
for i= 1:3

function y= f(x)
    y=8*%pi*h*c./(x^5.*(exp(h*c./(l*k*T(i)))-1))
endfunction

scf(0)
cv1=f(l)
plot(l,cv1,s(i))

E(i)=inttrap(l,cv1)*c/4  // energy  emitted for per unit area per second
y(i)=max(cv1)
n(i)=find(cv1==y(i))
x(i)=l(n(i))
disp("for T=" +string(T(i))+"K max nergy density is " +string(y(i))+"j/m^3")
disp("wavelength corresponding to max energy is "+string(x(i))+"m")
disp("wavelength*temp.="+string(x(i)*T(i))+"mk")
plot(x(i)*ones(1,length(l)),cv1,'-')
end
// stephan law
[a,b]=reglin(T^4,E')
sigma=a
disp("stephan constant is " +string(sigma))

function y=g(x)
    y= 8*%pi*h.*x^3./(c^3.*(exp(h.*x/(k*T(i)))-1))
endfunction

scf(1)
cv2=g(v)
plot(v,cv2,s(i))
