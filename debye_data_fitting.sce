clc;clear;clf
k=1.38e-23 // in joule per kelvin
h=6.6e-34   //in juole seconds
R=8.3103 //gas constant in Joule /kelvin/mole
//given data
T=[222.4,262.4,283.7,306.4,331.3,358.5,413,479.2,520,879.7,1079.7,1258]
//kelvin
C=[0.1278,0.1922,0.2271,0.2653,0.3082,0.3552,0.4463,0.5501,0.6089,0.8871,0.9034,0.9235]
//Fitting the einstein model
function y=ein(t, Te)
 y=Te^2*exp(Te./t)./(t^2.*(exp(Te./t)-1)^2)
endfunction
te=[1000:1:2000]
for i=1:length(te)
 for j=1:length(T)
 //deviation between given data and value obtained from planks formula for a particuular temperature
    er(i,j)=C(j)-ein(T(j),te(i))
 end
 //sum of sqaure of error
 s(i)=sum(er(i,:).*er(i,:)) 
end
n=find(s==min(s))
Te=te(n)
w=k*Te*2*%pi/h
disp("einstein angular frequency is "+string(w)+"m/s" )
disp("for Te="+string(Te)+"K einstein model least sqaure fits the given data")
plot(T,C,'or','linewidth',3)
T1=1:2*Te 
plot(T1,ein(T1,Te),'--b','linewidth',2)
// Fitting Debye model
function y=deb(t, Td)
 xd=Td./t
 y=3.*t^3./Td^3.*(integrate('exp(x).*x^4./(exp(x)-1)^2','x',0.1,xd))
endfunction
td=[1500:2000]
for i=1:length(td)
 for j=1:length(T)
 //deviation between given data and value obtained from planks formula 
//for a particuular temperature 
 er(i,j)=C(j)-deb(T(j),td(i))
 end
 //sum of sqaure of error
 s1(i)=sum(er(i,:).*er(i,:)) 
end
n1=find(s1==min(s1))
Td=td(n1)
w1=k*Td*2*%pi/h
disp("debye anular frequency is "+string(w1)+"m/s" )
disp("for Td="+string(Td)+"K debye model least sqaure fits the given data")
T2=100:2000
plot(T2,deb(T2,Td),'k','linewidth',2)
xtitle("Fitting the einstein model and debye model in given data","T(K)","Specific heat (J/k/mol)")
legend("given data","einstein fit curve","debye fit curve")
a=gca()
a.font_size=4
b=get("current_axes")
b.title.font_size=6
b.x_label.font_size=6
b.y_label.font_size=6
